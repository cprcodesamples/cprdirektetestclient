# CPR Proxy Test Client

## 0. Running the Proxy Test Client

The proxy test client requires that the .NET Framework 4.8 is installed (minimum Client Profile).

To run the proxy test client, you can load the included solution file in Visual Studio and Build/Run the CPRDirekteTestClient project. Note that before running the test client, you should ensure that your PNR Proxy and/or Match Proxy applications/Windows services (downloaded from https://cprservicedesk.atlassian.net/wiki/spaces/CPR/pages/11436095/CPR+Direkte+klient) are setup and running (if installation is blocked by Windows Defender, you can run a cmd prompt as administrator, and use that to run the setup file). Follow the Direkte proxy client documentation: https://cprservicedesk.atlassian.net/wiki/spaces/CPR/pages/11436129/Installations-+og+driftsvejledninger

## 1. Configuration

Before testing your CPR Proxy application/service, you need to fill in configuration data in the test client. Configuration is done via the "CPR Direkte proxy adresser" tab in the test client. Here you must provide the following:

1. IP address of the server where CPR Proxy application/service is running.
2. Local port on server where CPR Proxy application/service is running that will accept incoming connections from clients requesting data from CPR.

For more information regarding the the PNR and Match interfaces to CPR, read the technical documentation available on Confluence: https://cprservicedesk.atlassian.net/wiki/spaces/CPR/pages/11436178/CPR+Direkte