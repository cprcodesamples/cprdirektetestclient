﻿using System;
using System.Windows.Forms;

namespace CPRDirekteTestClient
{
    public partial class Form1 : Form
    {
        private ProxyHelper _helper = new ProxyHelper();
        public Form1()
        {
            InitializeComponent();

            txtPNRServer.Text = Properties.Settings.Default.PnrIP;
            txtPNRPort.Text = Properties.Settings.Default.PnrPORT;
            txtMatchServer.Text = Properties.Settings.Default.MatchIP;
            txtMatchPort.Text = Properties.Settings.Default.MatchPort;
        }

        private void btnSendPNR_Click(object sender, EventArgs e)
        {
            try
            {
                txtRequest.Text = _helper.CreatePNRTransaction(txtAbonnType.Text, txtDataType.Text, txtPNR.Text);

                txtResponse.Text = _helper.SendReceiveTransaction(txtPNRServer.Text, short.Parse(txtPNRPort.Text), txtRequest.Text);
            }
            catch (Exception ex)
            {
                txtResponse.Text = ex.ToString();
            }
            toolStripStatusTime.Text = _helper.TimeTaken.ToString() + "ms.";
        }

        private void btnSendMatch_Click(object sender, EventArgs e)
        {
            try
            {
                string msg = _helper.CreateMatchTransaction(txtAbonnType.Text, 
                                                            txtDataType.Text, 
                                                            tbKundeRefNr.Text,
                                                            tbKoen.Text,
                                                            tbNavn.Text,
                                                            tbFoedDag.Text,
                                                            tbFoedMd.Text,
                                                            tbFoedAar.Text,
                                                            tbPostDist.Text,
                                                            tbKommune.Text,
                                                            tbVej.Text,
                                                            tbHusNr.Text,
                                                            tbEtage.Text,
                                                            tbSideDoer.Text);

                txtMatchResponse.Text = _helper.SendReceiveTransaction(txtMatchServer.Text, short.Parse(txtMatchPort.Text), msg);
            }
            catch (Exception ex)
            {
                txtResponse.Text = ex.ToString();
            }
            toolStripStatusTime.Text = _helper.TimeTaken.ToString() + "ms.";
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Properties.Settings.Default.Save();
        }

        private void txtServer_TextChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.PnrIP = txtPNRServer.Text;
        }

        private void txtPort_TextChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.PnrPORT = txtPNRPort.Text;
        }

        private void txtMatchServer_TextChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.MatchIP = txtMatchServer.Text;
        }

        private void txtMatchPort_TextChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.MatchPort = txtMatchPort.Text;
        }
    }
}
