﻿using System;
using System.Diagnostics;
using System.Net.Sockets;
using System.Text;

namespace CPRDirekteTestClient
{           
    /// <summary>
    /// Class ProxyHelper is used to communicate with CPR Direkte Proxy.
    /// Implements two basic methods that will setup transaction string
    /// and send/receive the request/response.
    /// </summary>
    public class ProxyHelper
    {

        private long _timeTaken = 0;

        public long TimeTaken
        {
            get { return _timeTaken; }         
        }

        /// <summary>
        ///    Create a TcpClient.
        ///    Note, for this client to work you need to have a TcpServer 
        ///    connected to the same address as specified by the server, port
        ///    combination.
        /// </summary>
        /// <param name="serverIP"></param>
        /// <param name="Port"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public string SendReceiveTransaction(string serverIP, 
                                             short Port, 
                                             string message)
        {
            string rtn = string.Empty;
            try
            {
                Stopwatch sw = new Stopwatch();
                sw.Start();

                using (TcpClient client = new TcpClient(serverIP, Port))
                {
                    client.ReceiveTimeout = 5000;

                    // Translate the passed message into ASCII and store it as a Byte array.
                    byte[] sendData = Encoding.GetEncoding("iso-8859-1").GetBytes(message);

                    // Buffer for reading data
                    byte[] bytes = new byte[1024];

                    // String to store the response ASCII representation.
                    string responseData = string.Empty;

                    // Get a client stream for reading and writing.
                    // Stream stream = client.GetStream();
                    using (NetworkStream stream = client.GetStream())
                    {
                        // Send the message to the connected TcpServer. 
                        stream.Write(sendData, 0, sendData.Length);

                        // Read the first TcpServer response in chunks of 1024 bytes
                        int i = stream.Read(bytes, 0, bytes.Length);
                        while (i != 0)
                        {
                            responseData = Encoding.GetEncoding("iso-8859-1").GetString(bytes, 0, i);
                            rtn += responseData;
                            i = stream.Read(bytes, 0, bytes.Length);
                        }
                    }                    
                }
                
                sw.Stop();

                _timeTaken = sw.ElapsedMilliseconds;
                
            }
            catch (Exception e)
            {
                _timeTaken = 0;
                rtn = e.ToString();
            }
            return rtn;
        }

        /// <summary>
        /// Creates the message transaction for CPR PNR Direkte.
        /// Note: Its basically an empty string with a comma, but
        ///       the different parts of the string has been shown
        ///       on purpose to illustrate what part is filled out by
        ///       the Proxy server and what part this client uses.
        /// </summary>
        /// <param name="abbontype"></param>
        /// <param name="datatype"></param>
        /// <param name="CPRNummer"></param>
        /// <returns></returns>
        public string CreatePNRTransaction(string abbontype, 
                                           string datatype, 
                                           string CPRNummer)
        {
            //Tomme felter, som udfyldes af
            //CPR-direkte PNR Proxy-programmet
            string CPRtrans = "    ";       // 4 blanke
            string Komma = ",";             // 1 blank
            string Kundenr = "    ";        // 4 blanke
            string Token = "        ";      // 8 blanke
            string Userid = "        ";     // 8 blanke
            string Fejlnr = "  ";           // 2 blanke

            StringBuilder sb = new StringBuilder();

            sb.Append(CPRtrans);
            sb.Append(Komma);
            sb.Append(Kundenr);
            sb.Append(abbontype);
            sb.Append(datatype);
            sb.Append(Token);
            sb.Append(Userid);
            sb.Append(Fejlnr);
            sb.Append(CPRNummer);
            return sb.ToString();
        }

        /// <summary>
        /// Creates the message transaction for CPR Match Direkte.
        /// Note: Its basically an empty string with a comma, but
        ///       the different parts of the string has been shown
        ///       on purpose to illustrate what part is filled out by
        ///       the Proxy server and what part this client uses.
        /// </summary>
        /// <param name="abbontype"></param>
        /// <param name="datatype"></param>
        /// <param name="CPRNummer"></param>
        /// <returns></returns>
        public string CreateMatchTransaction(string abbontype, 
                                             string datatype, 
                                             string kundeRefNr,
                                             string koen,
                                             string navn,
                                             string foeddag,
                                             string foedmd,
                                             string foedaar,
                                             string postdist,
                                             string kommune,
                                             string vej,
                                             string husnr,
                                             string etage,
                                             string sidedoer)
        {
            
            //Tomme felter, som udfyldes af
            //CPR Direkte Match Proxy-programmet            
            string Kundenr = "    ";        // 4 blanke
            string Token = "        ";      // 8 blanke
            string Userid = "        ";     // 8 blanke
            string Fejlnr = "  ";           // 2 blanke
            string Filler = new StringBuilder().Append(' ', 70).ToString(); // 70 blanke

            StringBuilder sb = new StringBuilder();

            sb.Append(Kundenr);
            sb.Append(abbontype);
            sb.Append(datatype);
            sb.Append(Token);
            sb.Append(Userid);
            sb.Append(Fejlnr);
            sb.Append((kundeRefNr + Filler).Substring(0, 15));
            sb.Append((koen + Filler).Substring(0, 1));
            sb.Append((navn + Filler).Substring(0, 66));
            sb.Append((foeddag + Filler).Substring(0, 2));
            sb.Append((foedmd + Filler).Substring(0, 2));
            sb.Append((foedaar + Filler).Substring(0, 4));
            sb.Append((postdist + Filler).Substring(0, 20));
            sb.Append((kommune + Filler).Substring(0, 20));
            sb.Append((vej + Filler).Substring(0, 40));
            sb.Append((husnr + Filler).Substring(0, 4));
            sb.Append((etage + Filler).Substring(0, 2));
            sb.Append((sidedoer + Filler).Substring(0, 4));
            return sb.ToString();
        }
    }
}
