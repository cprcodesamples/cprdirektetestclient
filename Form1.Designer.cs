﻿namespace CPRDirekteTestClient
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusTime = new System.Windows.Forms.ToolStripStatusLabel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtResponse = new System.Windows.Forms.TextBox();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtRequest = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Label5 = new System.Windows.Forms.Label();
            this.Label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtPNR = new System.Windows.Forms.TextBox();
            this.txtDataType = new System.Windows.Forms.TextBox();
            this.txtAbonnType = new System.Windows.Forms.TextBox();
            this.btnSendPNR = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtMatchPort = new System.Windows.Forms.TextBox();
            this.txtMatchServer = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.txtPNRPort = new System.Windows.Forms.TextBox();
            this.txtPNRServer = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.btnSendMatch = new System.Windows.Forms.Button();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.Label17 = new System.Windows.Forms.Label();
            this.tbNavn = new System.Windows.Forms.TextBox();
            this.Label16 = new System.Windows.Forms.Label();
            this.Label15 = new System.Windows.Forms.Label();
            this.Label14 = new System.Windows.Forms.Label();
            this.Label13 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.tbSideDoer = new System.Windows.Forms.TextBox();
            this.tbEtage = new System.Windows.Forms.TextBox();
            this.tbHusNr = new System.Windows.Forms.TextBox();
            this.tbVej = new System.Windows.Forms.TextBox();
            this.tbKommune = new System.Windows.Forms.TextBox();
            this.tbPostDist = new System.Windows.Forms.TextBox();
            this.tbFoedAar = new System.Windows.Forms.TextBox();
            this.tbFoedMd = new System.Windows.Forms.TextBox();
            this.tbFoedDag = new System.Windows.Forms.TextBox();
            this.tbKoen = new System.Windows.Forms.TextBox();
            this.tbKundeRefNr = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.txtMatchResponse = new System.Windows.Forms.TextBox();
            this.statusStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusTime});
            this.statusStrip1.Location = new System.Drawing.Point(0, 468);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(549, 22);
            this.statusStrip1.SizingGrip = false;
            this.statusStrip1.TabIndex = 7;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusTime
            // 
            this.toolStripStatusTime.Name = "toolStripStatusTime";
            this.toolStripStatusTime.Size = new System.Drawing.Size(0, 17);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(549, 468);
            this.tabControl1.TabIndex = 8;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox3);
            this.tabPage2.Controls.Add(this.splitter1);
            this.tabPage2.Controls.Add(this.groupBox2);
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(541, 442);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Kald PNR Proxy";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtResponse);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(3, 263);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(535, 176);
            this.groupBox3.TabIndex = 29;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Response";
            // 
            // txtResponse
            // 
            this.txtResponse.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtResponse.Location = new System.Drawing.Point(3, 16);
            this.txtResponse.Multiline = true;
            this.txtResponse.Name = "txtResponse";
            this.txtResponse.ReadOnly = true;
            this.txtResponse.Size = new System.Drawing.Size(529, 157);
            this.txtResponse.TabIndex = 1;
            // 
            // splitter1
            // 
            this.splitter1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter1.Location = new System.Drawing.Point(3, 260);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(535, 3);
            this.splitter1.TabIndex = 27;
            this.splitter1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtRequest);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(3, 91);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(535, 169);
            this.groupBox2.TabIndex = 25;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Request";
            // 
            // txtRequest
            // 
            this.txtRequest.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRequest.Location = new System.Drawing.Point(3, 16);
            this.txtRequest.Multiline = true;
            this.txtRequest.Name = "txtRequest";
            this.txtRequest.ReadOnly = true;
            this.txtRequest.Size = new System.Drawing.Size(529, 150);
            this.txtRequest.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Label5);
            this.groupBox1.Controls.Add(this.Label4);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtPNR);
            this.groupBox1.Controls.Add(this.txtDataType);
            this.groupBox1.Controls.Add(this.txtAbonnType);
            this.groupBox1.Controls.Add(this.btnSendPNR);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(535, 88);
            this.groupBox1.TabIndex = 24;
            this.groupBox1.TabStop = false;
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Location = new System.Drawing.Point(168, 22);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(77, 13);
            this.Label5.TabIndex = 29;
            this.Label5.Text = "Personnummer";
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(109, 22);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(53, 13);
            this.Label4.TabIndex = 28;
            this.Label4.Text = "Data type";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(26, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 13);
            this.label6.TabIndex = 27;
            this.label6.Text = "Abon type";
            // 
            // txtPNR
            // 
            this.txtPNR.Location = new System.Drawing.Point(171, 46);
            this.txtPNR.Name = "txtPNR";
            this.txtPNR.Size = new System.Drawing.Size(96, 20);
            this.txtPNR.TabIndex = 26;
            // 
            // txtDataType
            // 
            this.txtDataType.Location = new System.Drawing.Point(112, 46);
            this.txtDataType.Name = "txtDataType";
            this.txtDataType.Size = new System.Drawing.Size(32, 20);
            this.txtDataType.TabIndex = 25;
            this.txtDataType.Text = "6";
            // 
            // txtAbonnType
            // 
            this.txtAbonnType.Location = new System.Drawing.Point(41, 47);
            this.txtAbonnType.Multiline = true;
            this.txtAbonnType.Name = "txtAbonnType";
            this.txtAbonnType.Size = new System.Drawing.Size(23, 19);
            this.txtAbonnType.TabIndex = 24;
            this.txtAbonnType.Text = "0";
            // 
            // btnSendPNR
            // 
            this.btnSendPNR.Location = new System.Drawing.Point(273, 38);
            this.btnSendPNR.Name = "btnSendPNR";
            this.btnSendPNR.Size = new System.Drawing.Size(57, 28);
            this.btnSendPNR.TabIndex = 23;
            this.btnSendPNR.Text = "Send";
            this.btnSendPNR.UseVisualStyleBackColor = true;
            this.btnSendPNR.Click += new System.EventHandler(this.btnSendPNR_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupBox6);
            this.tabPage3.Controls.Add(this.groupBox7);
            this.tabPage3.Controls.Add(this.groupBox4);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(541, 442);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Kald Match Proxy";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.txtMatchPort);
            this.tabPage1.Controls.Add(this.txtMatchServer);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.Label2);
            this.tabPage1.Controls.Add(this.Label1);
            this.tabPage1.Controls.Add(this.txtPNRPort);
            this.tabPage1.Controls.Add(this.txtPNRServer);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(541, 442);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "CPR Direkte proxy adresser";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 97);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(167, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "CPR Direkte Match proxy adresse";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(170, 121);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(26, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "Port";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 117);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(57, 13);
            this.label9.TabIndex = 12;
            this.label9.Text = "IP adresse";
            // 
            // txtMatchPort
            // 
            this.txtMatchPort.Location = new System.Drawing.Point(168, 135);
            this.txtMatchPort.Name = "txtMatchPort";
            this.txtMatchPort.Size = new System.Drawing.Size(91, 20);
            this.txtMatchPort.TabIndex = 11;
            this.txtMatchPort.Text = "6004";
            this.txtMatchPort.TextChanged += new System.EventHandler(this.txtMatchPort_TextChanged);
            // 
            // txtMatchServer
            // 
            this.txtMatchServer.Location = new System.Drawing.Point(6, 134);
            this.txtMatchServer.Name = "txtMatchServer";
            this.txtMatchServer.Size = new System.Drawing.Size(126, 20);
            this.txtMatchServer.TabIndex = 10;
            this.txtMatchServer.Text = "127.0.0.1";
            this.txtMatchServer.TextChanged += new System.EventHandler(this.txtMatchServer_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(160, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "CPR Direkte PNR proxy adresse";
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(170, 43);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(26, 13);
            this.Label2.TabIndex = 8;
            this.Label2.Text = "Port";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(6, 39);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(57, 13);
            this.Label1.TabIndex = 7;
            this.Label1.Text = "IP adresse";
            // 
            // txtPNRPort
            // 
            this.txtPNRPort.Location = new System.Drawing.Point(168, 57);
            this.txtPNRPort.Name = "txtPNRPort";
            this.txtPNRPort.Size = new System.Drawing.Size(91, 20);
            this.txtPNRPort.TabIndex = 6;
            this.txtPNRPort.Text = "6003";
            this.txtPNRPort.TextChanged += new System.EventHandler(this.txtPort_TextChanged);
            // 
            // txtPNRServer
            // 
            this.txtPNRServer.Location = new System.Drawing.Point(6, 56);
            this.txtPNRServer.Name = "txtPNRServer";
            this.txtPNRServer.Size = new System.Drawing.Size(126, 20);
            this.txtPNRServer.TabIndex = 5;
            this.txtPNRServer.Text = "127.0.0.1";
            this.txtPNRServer.TextChanged += new System.EventHandler(this.txtServer_TextChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Controls.Add(this.textBox4);
            this.groupBox4.Controls.Add(this.textBox5);
            this.groupBox4.Controls.Add(this.btnSendMatch);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox4.Location = new System.Drawing.Point(3, 3);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(535, 88);
            this.groupBox4.TabIndex = 25;
            this.groupBox4.TabStop = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(109, 22);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 13);
            this.label11.TabIndex = 28;
            this.label11.Text = "Data type";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(26, 22);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(55, 13);
            this.label12.TabIndex = 27;
            this.label12.Text = "Abon type";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(112, 46);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(32, 20);
            this.textBox4.TabIndex = 0;
            this.textBox4.Text = "6";
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(41, 47);
            this.textBox5.Multiline = true;
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(23, 19);
            this.textBox5.TabIndex = 24;
            this.textBox5.Text = "0";
            // 
            // btnSendMatch
            // 
            this.btnSendMatch.Location = new System.Drawing.Point(187, 41);
            this.btnSendMatch.Name = "btnSendMatch";
            this.btnSendMatch.Size = new System.Drawing.Size(57, 28);
            this.btnSendMatch.TabIndex = 2;
            this.btnSendMatch.Text = "Send";
            this.btnSendMatch.UseVisualStyleBackColor = true;
            this.btnSendMatch.Click += new System.EventHandler(this.btnSendMatch_Click);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.Label17);
            this.groupBox7.Controls.Add(this.tbNavn);
            this.groupBox7.Controls.Add(this.Label16);
            this.groupBox7.Controls.Add(this.Label15);
            this.groupBox7.Controls.Add(this.Label14);
            this.groupBox7.Controls.Add(this.Label13);
            this.groupBox7.Controls.Add(this.label18);
            this.groupBox7.Controls.Add(this.label19);
            this.groupBox7.Controls.Add(this.label20);
            this.groupBox7.Controls.Add(this.label21);
            this.groupBox7.Controls.Add(this.label22);
            this.groupBox7.Controls.Add(this.label23);
            this.groupBox7.Controls.Add(this.label24);
            this.groupBox7.Controls.Add(this.label25);
            this.groupBox7.Controls.Add(this.tbSideDoer);
            this.groupBox7.Controls.Add(this.tbEtage);
            this.groupBox7.Controls.Add(this.tbHusNr);
            this.groupBox7.Controls.Add(this.tbVej);
            this.groupBox7.Controls.Add(this.tbKommune);
            this.groupBox7.Controls.Add(this.tbPostDist);
            this.groupBox7.Controls.Add(this.tbFoedAar);
            this.groupBox7.Controls.Add(this.tbFoedMd);
            this.groupBox7.Controls.Add(this.tbFoedDag);
            this.groupBox7.Controls.Add(this.tbKoen);
            this.groupBox7.Controls.Add(this.tbKundeRefNr);
            this.groupBox7.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox7.Location = new System.Drawing.Point(3, 91);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(535, 201);
            this.groupBox7.TabIndex = 31;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Søgekriterier for person";
            // 
            // Label17
            // 
            this.Label17.AutoSize = true;
            this.Label17.Location = new System.Drawing.Point(28, 72);
            this.Label17.Name = "Label17";
            this.Label17.Size = new System.Drawing.Size(33, 13);
            this.Label17.TabIndex = 24;
            this.Label17.Text = "Navn";
            // 
            // tbNavn
            // 
            this.tbNavn.Location = new System.Drawing.Point(15, 91);
            this.tbNavn.MaxLength = 66;
            this.tbNavn.Name = "tbNavn";
            this.tbNavn.Size = new System.Drawing.Size(352, 20);
            this.tbNavn.TabIndex = 8;
            // 
            // Label16
            // 
            this.Label16.AutoSize = true;
            this.Label16.Location = new System.Drawing.Point(388, 60);
            this.Label16.Name = "Label16";
            this.Label16.Size = new System.Drawing.Size(17, 13);
            this.Label16.TabIndex = 7;
            this.Label16.Text = "År";
            // 
            // Label15
            // 
            this.Label15.AutoSize = true;
            this.Label15.Location = new System.Drawing.Point(339, 60);
            this.Label15.Name = "Label15";
            this.Label15.Size = new System.Drawing.Size(40, 13);
            this.Label15.TabIndex = 6;
            this.Label15.Text = "Måned";
            // 
            // Label14
            // 
            this.Label14.AutoSize = true;
            this.Label14.Location = new System.Drawing.Point(293, 58);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(27, 13);
            this.Label14.TabIndex = 5;
            this.Label14.Text = "Dag";
            // 
            // Label13
            // 
            this.Label13.AutoSize = true;
            this.Label13.Location = new System.Drawing.Point(293, 21);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(64, 13);
            this.Label13.TabIndex = 19;
            this.Label13.Text = "Fødselsdato";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(184, 21);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(50, 13);
            this.label18.TabIndex = 18;
            this.label18.Text = "Køn M/K";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(27, 21);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(68, 13);
            this.label19.TabIndex = 17;
            this.label19.Text = "Kunde ref. nr";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(195, 114);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(54, 13);
            this.label20.TabIndex = 16;
            this.label20.Text = "Kommune";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(21, 114);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(58, 13);
            this.label21.TabIndex = 15;
            this.label21.Text = "Postdistrikt";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(461, 152);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(46, 13);
            this.label22.TabIndex = 14;
            this.label22.Text = "Side dør";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(418, 153);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(35, 13);
            this.label23.TabIndex = 13;
            this.label23.Text = "Etage";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(375, 153);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(38, 13);
            this.label24.TabIndex = 12;
            this.label24.Text = "Hus nr";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(21, 153);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(22, 13);
            this.label25.TabIndex = 11;
            this.label25.Text = "Vej";
            // 
            // tbSideDoer
            // 
            this.tbSideDoer.Location = new System.Drawing.Point(464, 169);
            this.tbSideDoer.MaxLength = 2;
            this.tbSideDoer.Name = "tbSideDoer";
            this.tbSideDoer.Size = new System.Drawing.Size(32, 20);
            this.tbSideDoer.TabIndex = 15;
            // 
            // tbEtage
            // 
            this.tbEtage.Location = new System.Drawing.Point(421, 169);
            this.tbEtage.MaxLength = 2;
            this.tbEtage.Name = "tbEtage";
            this.tbEtage.Size = new System.Drawing.Size(32, 20);
            this.tbEtage.TabIndex = 14;
            // 
            // tbHusNr
            // 
            this.tbHusNr.Location = new System.Drawing.Point(373, 169);
            this.tbHusNr.MaxLength = 4;
            this.tbHusNr.Name = "tbHusNr";
            this.tbHusNr.Size = new System.Drawing.Size(42, 20);
            this.tbHusNr.TabIndex = 13;
            // 
            // tbVej
            // 
            this.tbVej.Location = new System.Drawing.Point(15, 169);
            this.tbVej.MaxLength = 40;
            this.tbVej.Name = "tbVej";
            this.tbVej.Size = new System.Drawing.Size(352, 20);
            this.tbVej.TabIndex = 12;
            // 
            // tbKommune
            // 
            this.tbKommune.Location = new System.Drawing.Point(197, 130);
            this.tbKommune.MaxLength = 20;
            this.tbKommune.Name = "tbKommune";
            this.tbKommune.Size = new System.Drawing.Size(170, 20);
            this.tbKommune.TabIndex = 11;
            // 
            // tbPostDist
            // 
            this.tbPostDist.Location = new System.Drawing.Point(15, 130);
            this.tbPostDist.MaxLength = 20;
            this.tbPostDist.Name = "tbPostDist";
            this.tbPostDist.Size = new System.Drawing.Size(158, 20);
            this.tbPostDist.TabIndex = 9;
            // 
            // tbFoedAar
            // 
            this.tbFoedAar.Location = new System.Drawing.Point(378, 35);
            this.tbFoedAar.MaxLength = 4;
            this.tbFoedAar.Name = "tbFoedAar";
            this.tbFoedAar.Size = new System.Drawing.Size(60, 20);
            this.tbFoedAar.TabIndex = 4;
            // 
            // tbFoedMd
            // 
            this.tbFoedMd.Location = new System.Drawing.Point(336, 35);
            this.tbFoedMd.MaxLength = 2;
            this.tbFoedMd.Name = "tbFoedMd";
            this.tbFoedMd.Size = new System.Drawing.Size(31, 20);
            this.tbFoedMd.TabIndex = 3;
            // 
            // tbFoedDag
            // 
            this.tbFoedDag.Location = new System.Drawing.Point(296, 35);
            this.tbFoedDag.MaxLength = 2;
            this.tbFoedDag.Name = "tbFoedDag";
            this.tbFoedDag.Size = new System.Drawing.Size(31, 20);
            this.tbFoedDag.TabIndex = 2;
            // 
            // tbKoen
            // 
            this.tbKoen.Location = new System.Drawing.Point(198, 35);
            this.tbKoen.MaxLength = 1;
            this.tbKoen.Name = "tbKoen";
            this.tbKoen.Size = new System.Drawing.Size(23, 20);
            this.tbKoen.TabIndex = 1;
            // 
            // tbKundeRefNr
            // 
            this.tbKundeRefNr.Location = new System.Drawing.Point(15, 35);
            this.tbKundeRefNr.MaxLength = 15;
            this.tbKundeRefNr.Name = "tbKundeRefNr";
            this.tbKundeRefNr.Size = new System.Drawing.Size(116, 20);
            this.tbKundeRefNr.TabIndex = 0;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.txtMatchResponse);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox6.Location = new System.Drawing.Point(3, 292);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(535, 147);
            this.groupBox6.TabIndex = 32;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Response";
            // 
            // txtMatchResponse
            // 
            this.txtMatchResponse.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMatchResponse.Location = new System.Drawing.Point(3, 16);
            this.txtMatchResponse.Multiline = true;
            this.txtMatchResponse.Name = "txtMatchResponse";
            this.txtMatchResponse.ReadOnly = true;
            this.txtMatchResponse.Size = new System.Drawing.Size(529, 128);
            this.txtMatchResponse.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(549, 490);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.statusStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "CPR Direkte test client";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtResponse;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtRequest;
        private System.Windows.Forms.GroupBox groupBox1;
        internal System.Windows.Forms.Label Label5;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.Label label6;
        internal System.Windows.Forms.TextBox txtPNR;
        internal System.Windows.Forms.TextBox txtDataType;
        internal System.Windows.Forms.TextBox txtAbonnType;
        internal System.Windows.Forms.Button btnSendPNR;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label3;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.TextBox txtPNRPort;
        internal System.Windows.Forms.TextBox txtPNRServer;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusTime;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label label7;
        internal System.Windows.Forms.Label label8;
        internal System.Windows.Forms.Label label9;
        internal System.Windows.Forms.TextBox txtMatchPort;
        internal System.Windows.Forms.TextBox txtMatchServer;
        internal System.Windows.Forms.GroupBox groupBox7;
        internal System.Windows.Forms.Label Label17;
        internal System.Windows.Forms.TextBox tbNavn;
        internal System.Windows.Forms.Label Label16;
        internal System.Windows.Forms.Label Label15;
        internal System.Windows.Forms.Label Label14;
        internal System.Windows.Forms.Label Label13;
        internal System.Windows.Forms.Label label18;
        internal System.Windows.Forms.Label label19;
        internal System.Windows.Forms.Label label20;
        internal System.Windows.Forms.Label label21;
        internal System.Windows.Forms.Label label22;
        internal System.Windows.Forms.Label label23;
        internal System.Windows.Forms.Label label24;
        internal System.Windows.Forms.Label label25;
        internal System.Windows.Forms.TextBox tbSideDoer;
        internal System.Windows.Forms.TextBox tbEtage;
        internal System.Windows.Forms.TextBox tbHusNr;
        internal System.Windows.Forms.TextBox tbVej;
        internal System.Windows.Forms.TextBox tbKommune;
        internal System.Windows.Forms.TextBox tbPostDist;
        internal System.Windows.Forms.TextBox tbFoedAar;
        internal System.Windows.Forms.TextBox tbFoedMd;
        internal System.Windows.Forms.TextBox tbFoedDag;
        internal System.Windows.Forms.TextBox tbKoen;
        internal System.Windows.Forms.TextBox tbKundeRefNr;
        private System.Windows.Forms.GroupBox groupBox4;
        internal System.Windows.Forms.Label label11;
        internal System.Windows.Forms.Label label12;
        internal System.Windows.Forms.TextBox textBox4;
        internal System.Windows.Forms.TextBox textBox5;
        internal System.Windows.Forms.Button btnSendMatch;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox txtMatchResponse;
    }
}

